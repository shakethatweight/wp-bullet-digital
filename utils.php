<?php
/**
 * Created by PhpStorm.
 * User: imac1
 * Date: 26/11/2016
 * Time: 19:39
 */

function cc_is_ajax() {
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}