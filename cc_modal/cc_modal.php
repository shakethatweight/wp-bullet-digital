<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 07/10/2014
 * Time: 11:51
 */

add_action( 'wp_ajax_nopriv_cc_modal_load', 'handle_cc_modal_request');
add_action( 'wp_ajax_cc_modal_load', 'handle_cc_modal_request');

function handle_cc_modal_request() {

	$name = $_REQUEST['modal'];
	if(file_exists(get_template_directory() . '/templates/modals/' . $name . '.php')) {
		get_template_part('templates/modals/' . $name);
	} else {
		header("HTTP/1.0 404 Not Found");
		die('404 Not Found');
	}
	exit;

}


add_action( 'wp_enqueue_scripts', function() {

	wp_enqueue_script( 'cc_modal_js_spin', plugins_url('spin.js', __FILE__), array(), null, true );

	wp_register_script( 'cc_modal_js', plugins_url('cc_modal.js', __FILE__), array(), null, true );

	$cc_data = array(
		'ajax_url' => admin_url('admin-ajax.php'),
		'template_url' => get_template_directory_uri()
	);

	wp_localize_script( 'sage_js', 'cc_data', $cc_data);
	wp_localize_script( 'sage/js', 'cc_data', $cc_data);
	wp_enqueue_script('cc_modal_js');

}, 101 );